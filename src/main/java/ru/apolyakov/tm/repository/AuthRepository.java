package ru.apolyakov.tm.repository;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IAuthRepository;

public final class AuthRepository implements IAuthRepository {

    @Getter
    @Setter
    @Nullable
    private String userId;

}
