package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-finish-by-index";

    @NotNull
    private final static String DESCRIPTION = "Finishing project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        throwExceptionIfNull(getProjectService().completeByIndex(index - 1));
    }

}
