package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-update-by-id";

    @NotNull
    private final static String DESCRIPTION = "Update project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = TerminalUtil.readLine(ID_INPUT);
        @NotNull final String name = TerminalUtil.readLine(NAME_INPUT);
        @NotNull final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getProjectService().updateById(id, name, description));
    }

}
