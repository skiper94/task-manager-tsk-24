package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-update-by-index";

    @NotNull
    private final static String DESCRIPTION = "Update project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = TerminalUtil.readNumber(INDEX_INPUT);
        @NotNull final String name = TerminalUtil.readLine(NAME_INPUT);
        @NotNull final String description = TerminalUtil.readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getProjectService().updateByIndex(index - 1, name, description));
    }

}
