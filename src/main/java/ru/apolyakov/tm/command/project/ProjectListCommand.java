package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

import java.util.Comparator;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "projects-list";

    @NotNull
    private final static String DESCRIPTION = "Showing all projects";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void execute() {
        if (getProjectService().isEmpty()) {
            TerminalUtil.printLinesWithEmptyLine("No projects found. Type <project-create> to add a project.");
            return;
        }
        @NotNull final Comparator<Project> comparator = TerminalUtil.readComparator();
        TerminalUtil.printListWithIndexes(getProjectService().findAll(comparator));
    }

}
