package ru.apolyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.model.Project;
import ru.apolyakov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final static String NAME = "project-show-by-id";

    @NotNull
    private final static String DESCRIPTION = "Showing project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = TerminalUtil.readLine(ID_INPUT);
        @Nullable final Project project = getProjectService().findOneById(id);
        showProject(project);
    }

}
