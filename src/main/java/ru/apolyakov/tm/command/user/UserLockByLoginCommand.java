package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.security.UserSelfLockNotAllowedException;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-lock-by-login";

    @NotNull
    private static final String DESCRIPTION = "Locking user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        if (login.equals(getAuthService().getUserLogin())) throw new UserSelfLockNotAllowedException();
        if (!getUserService().lockByLogin(login)) printLinesWithEmptyLine(USER_ALREADY_LOCKED);
    }

}
