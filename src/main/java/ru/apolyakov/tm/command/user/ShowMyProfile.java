package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.security.AccessDeniedNotAuthorizedException;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.ValidationUtil.isEmptyStr;

public final class ShowMyProfile extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-profile-show";

    @NotNull
    private static final String DESCRIPTION = "Printing current user profile";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        if (isEmptyStr(getAuthService().getUserId())) throw new AccessDeniedNotAuthorizedException();
        printLinesWithEmptyLine(getUserService().findOneById(getAuthService().getUserId()));
    }

}
