package ru.apolyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.security.UserSelfLockNotAllowedException;

import static ru.apolyakov.tm.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class UserLockByIdCommand extends AbstractUserCommand {

    @NotNull
    private static final String NAME = "user-lock-by-id";

    @NotNull
    private static final String DESCRIPTION = "Locking user by ID";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        if (id.equals(getAuthService().getUserId())) throw new UserSelfLockNotAllowedException();
        if (!getUserService().lockById(id)) printLinesWithEmptyLine(USER_ALREADY_LOCKED);
    }

}
