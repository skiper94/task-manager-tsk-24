package ru.apolyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.command.AbstractCommand;

import static ru.apolyakov.tm.util.ValidationUtil.isEmptyStr;

public final class ListCommandsCommand extends AbstractCommand {

    @NotNull
    private final static String CMD_NAME = "arguments-list";

    @NotNull
    private final static String DESCRIPTION = "List available command-line arguments";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @Override
    public final void execute() {
        System.out.println();
        for (@NotNull final AbstractCommand command : getCommandService().getCommandList()) {
            if (isEmptyStr(command.getArgument())) continue;
            System.out.println(command.getArgument());
        }
        System.out.println();
    }

}
