package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-update-by-id";

    @NotNull
    private final static String DESCRIPTION = "Updating task by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        throwExceptionIfNull(getTaskService().updateById(id, name, description));
    }

}
