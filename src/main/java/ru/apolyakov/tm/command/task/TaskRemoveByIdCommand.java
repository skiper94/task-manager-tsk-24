package ru.apolyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.apolyakov.tm.util.TerminalUtil.readLine;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final static String NAME = "task-remove-by-id";

    @NotNull
    private final static String DESCRIPTION = "Removing task by ID";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getTaskService().removeOneById(id));
    }

}
