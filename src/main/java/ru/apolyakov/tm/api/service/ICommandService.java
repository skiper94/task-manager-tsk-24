package ru.apolyakov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.command.AbstractCommand;

import java.util.List;
import java.util.Map;

public interface ICommandService {

    @NotNull Map<String, AbstractCommand> getArguments();

    @NotNull List<AbstractCommand> getCommandList();

    @NotNull Map<String, AbstractCommand> getCommands();

}
