package ru.apolyakov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class NameEmptyException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Name is empty.";

    public NameEmptyException() {
        super(MESSAGE);
    }

}
