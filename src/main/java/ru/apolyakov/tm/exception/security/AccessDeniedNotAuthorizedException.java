package ru.apolyakov.tm.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class AccessDeniedNotAuthorizedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Operation unavailable for non-authorized users.";

    public AccessDeniedNotAuthorizedException() {
        super(MESSAGE);
    }

}
