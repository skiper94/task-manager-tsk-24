package ru.apolyakov.tm.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.exception.AbstractException;

public final class UserSelfDeleteNotAllowedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Cannot remove logged-on user.";

    public UserSelfDeleteNotAllowedException() {
        super(MESSAGE);
    }

}
