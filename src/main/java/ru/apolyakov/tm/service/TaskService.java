package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.ITaskRepository;
import ru.apolyakov.tm.api.service.IAuthService;
import ru.apolyakov.tm.api.service.ITaskService;
import ru.apolyakov.tm.exception.security.AccessDeniedNotAuthorizedException;
import ru.apolyakov.tm.model.Task;
import ru.apolyakov.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository repository;

    public TaskService(@NotNull final ITaskRepository repository, @NotNull final IAuthService authService) {
        super(repository, authService);
        this.repository = repository;
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String taskId, @NotNull final String projectId) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.addTaskToProject(taskId, projectId);
        return repository.addTaskToProjectLimitByUser(authService.getUserId(), taskId, projectId);
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final Comparator<Task> comparator) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.findAllByProjectId(projectId, comparator);
        return repository.findAllByProjectIdLimitByUser(authService.getUserId(), projectId, comparator);
    }

    @Override
    public final void removeAllByProjectId(@NotNull final String projectId) {
        repository.removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String taskId, @NotNull final String projectId) {
        if (ValidationUtil.isEmptyStr(authService.getUserId())) throw new AccessDeniedNotAuthorizedException();
        if (authService.isPrivilegedUser()) return repository.removeTaskFromProject(taskId, projectId);
        return repository.removeTaskFromProjectLimitByUser(authService.getUserId(), taskId, projectId);
    }

}
