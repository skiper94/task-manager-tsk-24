package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.apolyakov.tm.api.repository.ICommandRepository;
import ru.apolyakov.tm.api.service.ICommandService;
import ru.apolyakov.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public final Map<String, AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    @Override
    public final List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commandRepository.getCommands().values());
    }

    @NotNull
    @Override
    public final Map<String, AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}
