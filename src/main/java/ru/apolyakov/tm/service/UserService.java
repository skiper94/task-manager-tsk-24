package ru.apolyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.apolyakov.tm.api.repository.IUserRepository;
import ru.apolyakov.tm.api.service.IUserService;
import ru.apolyakov.tm.enumerated.Role;
import ru.apolyakov.tm.exception.empty.EmailEmptyException;
import ru.apolyakov.tm.exception.empty.IdEmptyException;
import ru.apolyakov.tm.exception.empty.LoginEmptyException;
import ru.apolyakov.tm.exception.empty.PasswordEmptyException;
import ru.apolyakov.tm.exception.entity.UserExistsWIthEmailException;
import ru.apolyakov.tm.exception.entity.UserExistsWithLoginException;
import ru.apolyakov.tm.exception.entity.UserNotFoundException;
import ru.apolyakov.tm.model.User;
import ru.apolyakov.tm.util.HashUtil;
import ru.apolyakov.tm.util.ValidationUtil;

import java.util.Objects;
import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        if (ValidationUtil.isEmptyStr(password)) throw new PasswordEmptyException();
        if (ValidationUtil.isEmptyStr(email)) throw new EmailEmptyException();
        if (userRepository.isFoundByLogin(login)) throw new UserExistsWithLoginException(login);
        if (userRepository.isFoundByEmail(email)) throw new UserExistsWIthEmailException(email);
        userRepository.add(new User(login, Objects.requireNonNull(HashUtil.salt(password)), email, firstName, middleName, lastName, role));
    }

    @Nullable
    @Override
    public final User findByLogin(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return findOneById(id);
    }

    @Nullable
    private String findIdByLogin(@NotNull final String login) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        return userRepository.findIdByLogin(login);
    }

    @Override
    public final boolean lockById(@NotNull final String id) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findOneById(id)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked()) return false;
        user.setLocked(true);
        return true;
    }

    @Override
    public final boolean lockByLogin(@NotNull final String login) {
        if (ValidationUtil.isEmptyStr(login)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (user.isLocked()) return false;
        user.setLocked(true);
        return true;
    }

    @Nullable
    @Override
    public final User removeByLogin(@NotNull final String login) {
        @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return removeOneById(id);
    }

    @Override
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        if (ValidationUtil.isEmptyStr(password)) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
    }

    @Override
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setRole(role);
    }

    @Override
    public final boolean unlockById(@NotNull final String id) {
        if (ValidationUtil.isEmptyStr(id)) throw new IdEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findOneById(id)).orElseThrow(UserNotFoundException::new);
        if (!user.isLocked()) return false;
        user.setLocked(false);
        return true;
    }

    @Override
    public final boolean unlockByLogin(@NotNull final String login) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        @NotNull final User user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (!user.isLocked()) return false;
        user.setLocked(false);
        return true;
    }

    @Nullable
    @Override
    public final User updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                 @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (ValidationUtil.isEmptyStr(login)) throw new LoginEmptyException();
        if (ValidationUtil.isEmptyStr(password)) throw new PasswordEmptyException();
        if (ValidationUtil.isEmptyStr(email)) throw new EmailEmptyException();
        @Nullable final User userFoundWithThisLogin = userRepository.findByLogin(login);
        @Nullable final User userFoundWithThisEmail = userRepository.findByEmail(email);
        if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
            throw new UserExistsWithLoginException(login);
        if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
            throw new UserExistsWIthEmailException(email);
        return userRepository.update(id, login, password, email, role, firstName, middleName, lastName);
    }

    @Nullable
    @Override
    public final User updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        @NotNull final String id = Optional.ofNullable(userRepository.findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
        return updateById(id, login, password, email, role, firstName, middleName, lastName);
    }

}
